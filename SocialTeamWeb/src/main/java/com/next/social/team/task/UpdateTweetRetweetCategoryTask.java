package com.next.social.team.task;

import java.util.concurrent.Callable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.next.social.team.nodes.Category;
import com.next.social.team.nodes.Tweet;
import com.next.social.team.repo.service.SocialTeamService;

public class UpdateTweetRetweetCategoryTask implements Callable<Tweet> {
    private Tweet tweet;
    private Category category;
    private SocialTeamService socialTeamService;
    Logger logger = LoggerFactory.getLogger(this.getClass());

    public UpdateTweetRetweetCategoryTask(SocialTeamService socialTeamService, Tweet tweet, Category category) {
        this.socialTeamService = socialTeamService;
        this.tweet = tweet;
        this.category = category;
    }

    @Override
    public Tweet call() throws Exception {
        logger.info("Updaing Category to {} for all retweets if tweet {}", category.getName(), tweet.getText());
        tweet = socialTeamService.updateTweetsRetweetCategory(tweet, category);
        return tweet;
    }

}
