/**
 *  (C) 2013-2014 Stephan Rauh http://www.beyondjava.net
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.next.social.team.jsf.beans;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.primefaces.event.SelectEvent;
import org.primefaces.model.mindmap.DefaultMindmapNode;
import org.primefaces.model.mindmap.MindmapNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;

import com.next.social.team.dto.HashTagWithCount;
import com.next.social.team.exceptions.AppException;
import com.next.social.team.nodes.HashTag;
import com.next.social.team.repo.service.SocialTeamService;

@Component
@Scope(proxyMode = ScopedProxyMode.TARGET_CLASS, value = "session")
public class RelatedHashtagBean {

    @Autowired
    private SocialTeamService socialTeamService;

    private MindmapNode root;

    private MindmapNode selectedNode;

    private String hashtag;

    private List<HashTagWithCount> hashTags;


    protected void sendInfoMessage(String message) {
        FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO, "", message);
        FacesContext.getCurrentInstance().addMessage("", fm);
    }

    protected void sendErrorMessage(String message) {
        FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR, "", message);
        FacesContext.getCurrentInstance().addMessage("", fm);
    }

    public void reloadDataForhashtag() {

        try {
            HashTag selectedhashTag = socialTeamService.getHashTagById(hashtag);
            if (selectedhashTag != null) {
                root = new DefaultMindmapNode(selectedhashTag.getTag(), selectedhashTag, "FFCC00", true);
                loadRelatedhashtags(root, selectedhashTag);
            }

        } catch (AppException e) {
            e.printStackTrace();
        }
    }

   public void showErrors() {
	   FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO, "", "The low-priority message is displayed.");
	   FacesContext.getCurrentInstance().addMessage("smallNumberID", fm);
	   fm = new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "This error message is oppressed, although it seems to be more important.");
	   FacesContext.getCurrentInstance().addMessage("smallNumberID", fm);
   }

    public void onNodeSelect(SelectEvent event) {
        MindmapNode node = (MindmapNode) event.getObject();

        // populate if not already loaded
        if (node.getChildren().isEmpty()) {
            HashTag tag = (HashTag) node.getData();
            loadRelatedhashtags(node, tag);
        }
    }

    private void loadRelatedhashtags(MindmapNode node, HashTag tag) {

        hashtag = tag.getTag();
        try {
            hashTags = socialTeamService.getRelatedHashTags(tag.getId());
            for (HashTagWithCount oneHashTag : hashTags) {
                node.addNode(new DefaultMindmapNode(oneHashTag.getHashTag().getTag() + " : " + oneHashTag.getCount(), oneHashTag.getHashTag(), "82c542", true));
            }
        } catch (AppException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public void onNodeDblselect(SelectEvent event) {
        this.selectedNode = (MindmapNode) event.getObject();
    }

    public MindmapNode getRoot() {
        return root;
    }

    public void setRoot(MindmapNode root) {
        this.root = root;
    }

    public MindmapNode getSelectedNode() {
        return selectedNode;
    }

    public void setSelectedNode(MindmapNode selectedNode) {
        this.selectedNode = selectedNode;
    }

    public String getHashtag() {
        return hashtag;
    }

    public void setHashtag(String hashtag) {
        this.hashtag = hashtag;
    }

    public List<HashTagWithCount> getHashTags() {
        return hashTags;
    }

    public void setHashTags(List<HashTagWithCount> hashTags) {
        this.hashTags = hashTags;
    }

}