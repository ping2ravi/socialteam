/**
 *  (C) 2013-2014 Stephan Rauh http://www.beyondjava.net
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.next.social.team.jsf.beans;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;

import com.next.social.team.exceptions.AppException;
import com.next.social.team.nodes.Category;
import com.next.social.team.repo.service.SocialTeamService;

@Component
@Scope(proxyMode = ScopedProxyMode.TARGET_CLASS, value = "session")
public class CategoriesBean {

    private List<Category> categories;

    @Autowired
    private SocialTeamService socialTeamService;

    private Category selectedCategory;

    private boolean showList = true;

    @PostConstruct
    public void init() {
        refreshCategories();
    }

    private void refreshCategories(){
        try {
            categories = socialTeamService.getAllCategories();
        } catch (AppException e) {
            FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Unable to get Tracking Categorys from Database.");
            FacesContext.getCurrentInstance().addMessage("", fm);
            e.printStackTrace();
        }
    }

    public void createCategory() {
        System.out.println("Creating New term");
        showList = false;
        selectedCategory = new Category();
    }

    public void cancel() {
        System.out.println("cancel");
        showList = true;
    }

    public void saveCategory() {
        try {
            socialTeamService.saveCategory(selectedCategory);
            sendInfoMessage("Category saved");
            showList = true;
            refreshCategories();
        } catch (AppException e) {
            e.printStackTrace();
            sendErrorMessage("Unable to save Category : " + e.getMessage());
        }
    }

    protected void sendInfoMessage(String message) {
        FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO, "", message);
        FacesContext.getCurrentInstance().addMessage("", fm);
    }

    protected void sendErrorMessage(String message) {
        FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR, "", message);
        FacesContext.getCurrentInstance().addMessage("", fm);
    }

   public void showErrors() {
	   FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO, "", "The low-priority message is displayed.");
	   FacesContext.getCurrentInstance().addMessage("smallNumberID", fm);
	   fm = new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "This error message is oppressed, although it seems to be more important.");
	   FacesContext.getCurrentInstance().addMessage("smallNumberID", fm);
   }

    public List<Category> getCategories() {
        return categories;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

    public Category getSelectedCategory() {
        return selectedCategory;
    }

    public void setSelectedCategory(Category selectedCategory) {
        this.selectedCategory = selectedCategory;
        showList = false;
    }

    public boolean isShowList() {
        return showList;
    }

    public void setShowList(boolean showList) {
        this.showList = showList;
    }

}