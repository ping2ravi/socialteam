/**
 *  (C) 2013-2014 Stephan Rauh http://www.beyondjava.net
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.next.social.team.jsf.beans;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;

import com.next.social.team.exceptions.AppException;
import com.next.social.team.nodes.TrackingTerm;
import com.next.social.team.repo.service.SocialTeamService;

@Component
@Scope(proxyMode = ScopedProxyMode.TARGET_CLASS, value = "session")
public class TermsBean {

    private List<TrackingTerm> trackingTerms;

    @Autowired
    private SocialTeamService socialTeamService;

    private TrackingTerm selectedTerm;

    private boolean showList = true;

    @PostConstruct
    public void init() {
        refreshTrackingTerms();
    }

    private void refreshTrackingTerms(){
        try {
            trackingTerms = socialTeamService.getAllTrackingTerms();
        } catch (AppException e) {
            FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Unable to get Tracking Terms from Database.");
            FacesContext.getCurrentInstance().addMessage("", fm);
            e.printStackTrace();
        }
    }

    public void createTerm() {
        System.out.println("Creating New term");
        showList = false;
        selectedTerm = new TrackingTerm();
    }

    public void cancel() {
        System.out.println("cancel");
        showList = true;
    }

    public void saveTerm() {
        try {
            socialTeamService.saveTrackingTerm(selectedTerm);
            sendInfoMessage("Term saved");
            showList = true;
            refreshTrackingTerms();
        } catch (AppException e) {
            e.printStackTrace();
            sendErrorMessage("Unable to save Term : " + e.getMessage());
        }
    }

    protected void sendInfoMessage(String message) {
        FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO, "", message);
        FacesContext.getCurrentInstance().addMessage("", fm);
    }

    protected void sendErrorMessage(String message) {
        FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR, "", message);
        FacesContext.getCurrentInstance().addMessage("", fm);
    }

   public void showErrors() {
	   FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO, "", "The low-priority message is displayed.");
	   FacesContext.getCurrentInstance().addMessage("smallNumberID", fm);
	   fm = new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "This error message is oppressed, although it seems to be more important.");
	   FacesContext.getCurrentInstance().addMessage("smallNumberID", fm);
   }

    public List<TrackingTerm> getTrackingTerms() {
        return trackingTerms;
    }

    public void setTrackingTerms(List<TrackingTerm> trackingTerms) {
        this.trackingTerms = trackingTerms;
    }

    public TrackingTerm getSelectedTerm() {
        return selectedTerm;
    }

    public void setSelectedTerm(TrackingTerm selectedTerm) {
        this.selectedTerm = selectedTerm;
        showList = false;
    }

    public boolean isShowList() {
        return showList;
    }

    public void setShowList(boolean showList) {
        this.showList = showList;
    }

}