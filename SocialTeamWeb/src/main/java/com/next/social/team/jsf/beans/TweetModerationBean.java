/**
 *  (C) 2013-2014 Stephan Rauh http://www.beyondjava.net
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.next.social.team.jsf.beans;

import java.util.List;
import java.util.concurrent.Future;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;

import com.next.social.team.exceptions.AppException;
import com.next.social.team.nodes.Category;
import com.next.social.team.nodes.Tweet;
import com.next.social.team.repo.service.SocialTeamService;
import com.next.social.team.task.UpdateTweetRetweetCategoryTask;
import com.next.social.team.task.UpdateUserCategoryTask;

@Component
@Scope(proxyMode = ScopedProxyMode.TARGET_CLASS, value = "session")
public class TweetModerationBean {

    private List<Tweet> tweets;

    @Autowired
    private CategoriesBean categoriesBean;

    @Autowired
    private SocialTeamService socialTeamService;

    @Autowired
    private ThreadPoolTaskExecutor threadPoolTaskExecutor;

    private Tweet selectedTweet;

    private List<Tweet> selectedTweets;


    @PostConstruct
    public void init() {
        refreshTweets();
    }

    private void refreshTweets() {
        try {
            tweets = socialTeamService.getTweetsNeedModeration();
        } catch (AppException e) {
            FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Unable to get Tracking Tweets from Database.");
            FacesContext.getCurrentInstance().addMessage("", fm);
            e.printStackTrace();
        }
    }

    public void updateCategoryAsAgainstAap() {
        if (selectedTweets == null || selectedTweets.isEmpty()) {
            sendErrorMessage("Please select one or more tweets");
            return;
        }
        for (Tweet tweet : selectedTweets) {
            updateTweetCategory(tweet, "Against Aap");
        }

        refreshTweets();
    }

    public void updateCategoryAsNeutral() {
        if (selectedTweets == null || selectedTweets.isEmpty()) {
            sendErrorMessage("Please select one or more tweets");
            return;
        }
        for (Tweet tweet : selectedTweets) {
            updateTweetCategory(tweet, "Neutral");
        }
        refreshTweets();
    }

    public void updateUserAsNeutral(String screenName) {
        updateUserCategory(screenName, "Neutral");
        refreshTweets();
    }

    public void updateUserAsAgainstAap(String screenName) {
        updateUserCategory(screenName, "Against Aap");
        refreshTweets();
    }

    public void updateCategoryAsProAap() {
        if (selectedTweets == null || selectedTweets.isEmpty()) {
            sendErrorMessage("Please select one or more tweets");
            return;
        }
        for (Tweet tweet : selectedTweets) {
            updateTweetCategory(tweet, "Pro Aap");
        }
        refreshTweets();
    }

    public void updateTweetCategory(Tweet tweet, String categoryName) {
        System.out.println("tweet : " + tweet + ",category=" + categoryName);
        Category category = getCategory(categoryName);
        if (category == null) {
            System.out.println("No category Found=" + categoryName);
            sendErrorMessage("No category Found");
            return;
        }
        try {
            tweet = socialTeamService.updateTweetCategory(tweet, category);
            UpdateTweetRetweetCategoryTask updateTweetRetweetCategoryTask = new UpdateTweetRetweetCategoryTask(socialTeamService, tweet, category);
            threadPoolTaskExecutor.submit(updateTweetRetweetCategoryTask);
        } catch (AppException e) {
            e.printStackTrace();
        }
    }

    private void updateUserCategory(String screenName, String categoryName) {
        System.out.println("screenName : " + screenName);
        Category category = getCategory(categoryName);
        if (category == null) {
            sendErrorMessage("No category Found");
            return;
        }
        try {
            UpdateUserCategoryTask updateUserCategoryTask = new UpdateUserCategoryTask(socialTeamService, screenName, category);
            Future<List<Tweet>> tweets = threadPoolTaskExecutor.submit(updateUserCategoryTask);
            for (Tweet oneTweet : tweets.get()) {
                UpdateTweetRetweetCategoryTask updateTweetRetweetCategoryTask = new UpdateTweetRetweetCategoryTask(socialTeamService, oneTweet, category);
                threadPoolTaskExecutor.submit(updateTweetRetweetCategoryTask);
            }
            sendInfoMessage("Total Tweets updated = " + tweets.get().size());
        } catch (Exception e) {
            e.printStackTrace();
            sendInfoMessage("Failed");
        }
        refreshTweets();
    }

    public void updateUserAsProAap(String screenName) {
        updateUserCategory(screenName, "Pro Aap");
    }

    private Category getCategory(String category) {
        List<Category> categories = categoriesBean.getCategories();
        Category selectedCategory = null;
        for (Category oneCategory : categories) {
            if (oneCategory.getName().equalsIgnoreCase(category)) {
                selectedCategory = oneCategory;
                break;
            }
        }
        return selectedCategory;
    }

    protected void sendInfoMessage(String message) {
        FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO, "", message);
        FacesContext.getCurrentInstance().addMessage("", fm);
    }

    protected void sendErrorMessage(String message) {
        FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR, "", message);
        FacesContext.getCurrentInstance().addMessage("", fm);
    }

   public void showErrors() {
	   FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO, "", "The low-priority message is displayed.");
	   FacesContext.getCurrentInstance().addMessage("smallNumberID", fm);
	   fm = new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "This error message is oppressed, although it seems to be more important.");
	   FacesContext.getCurrentInstance().addMessage("smallNumberID", fm);
   }

    public List<Tweet> getTweets() {
        return tweets;
    }

    public void setTweets(List<Tweet> tweets) {
        this.tweets = tweets;
    }

    public SocialTeamService getSocialTeamService() {
        return socialTeamService;
    }

    public void setSocialTeamService(SocialTeamService socialTeamService) {
        this.socialTeamService = socialTeamService;
    }

    public Tweet getSelectedTweet() {
        return selectedTweet;
    }

    public void setSelectedTweet(Tweet selectedTweet) {
        this.selectedTweet = selectedTweet;
    }

    public List<Tweet> getSelectedTweets() {
        return selectedTweets;
    }

    public void setSelectedTweets(List<Tweet> selectedTweets) {
        this.selectedTweets = selectedTweets;
    }

}