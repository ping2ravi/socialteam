/**
 *  (C) 2013-2014 Stephan Rauh http://www.beyondjava.net
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.next.social.team.jsf.beans;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;

import com.next.social.team.dto.TwitterAccountWithCount;
import com.next.social.team.exceptions.AppException;
import com.next.social.team.repo.service.SocialTeamService;

@Component
@Scope(proxyMode = ScopedProxyMode.TARGET_CLASS, value = "session")
public class HashtagAccountBean {

    @Autowired
    private SocialTeamService socialTeamService;

    private String hashtag;

    private List<TwitterAccountWithCount> accounts;

    public void reloadDataForhashtag() {
        if (hashtag == null || hashtag.trim().equals("")) {
            sendErrorMessage("Please enter a Hashtag");
            return;
        }
        try {
            accounts = socialTeamService.getTwitterAccountForHashTag(hashtag);
        } catch (AppException e) {
            e.printStackTrace();
        }
    }

    protected void sendInfoMessage(String message) {
        FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO, "", message);
        FacesContext.getCurrentInstance().addMessage("", fm);
    }

    protected void sendErrorMessage(String message) {
        FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR, "", message);
        FacesContext.getCurrentInstance().addMessage("", fm);
    }

   public void showErrors() {
	   FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO, "", "The low-priority message is displayed.");
	   FacesContext.getCurrentInstance().addMessage("smallNumberID", fm);
	   fm = new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "This error message is oppressed, although it seems to be more important.");
	   FacesContext.getCurrentInstance().addMessage("smallNumberID", fm);
   }


    public String getHashtag() {
        return hashtag;
    }

    public void setHashtag(String hashtag) {
        this.hashtag = hashtag;
    }

    public List<TwitterAccountWithCount> getAccounts() {
        return accounts;
    }

    public void setAccounts(List<TwitterAccountWithCount> accounts) {
        this.accounts = accounts;
    }

}