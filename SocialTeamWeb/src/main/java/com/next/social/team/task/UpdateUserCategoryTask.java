package com.next.social.team.task;

import java.util.List;
import java.util.concurrent.Callable;

import com.next.social.team.nodes.Category;
import com.next.social.team.nodes.Tweet;
import com.next.social.team.nodes.TwitterAccount;
import com.next.social.team.repo.service.SocialTeamService;

public class UpdateUserCategoryTask implements Callable<List<Tweet>> {
    private String screenName;
    private Category category;
    private SocialTeamService socialTeamService;

    public UpdateUserCategoryTask(SocialTeamService socialTeamService, String screenName, Category category) {
        this.socialTeamService = socialTeamService;
        this.screenName = screenName;
        this.category = category;
    }

    @Override
    public List<Tweet> call() throws Exception {
        TwitterAccount twitterAccount = socialTeamService.updateTwitterAccountCategory(screenName, category);
        List<Tweet> tweets = socialTeamService.updateUserAccountTweetCategory(twitterAccount, category);
        return tweets;
    }

}
