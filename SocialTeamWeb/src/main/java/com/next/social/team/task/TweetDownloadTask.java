package com.next.social.team.task;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import com.next.social.team.exceptions.AppException;
import com.next.social.team.repo.service.SocialTeamService;
import com.next.social.team.util.TwitterUtil;

@Component
public class TweetDownloadTask {

    @Autowired
    private TwitterUtil twitterUtil;

    @Autowired
    private SocialTeamService socialTeamService;

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    private JsonParser jsonParser = new JsonParser();

    private int count = 0;

    @Scheduled(fixedDelay = 300000)
    public void downloadTweets() {
        logger.info("Start downloadTweets");
        try {
            while (true) {
                String msg = twitterUtil.getNextTweetMessageWithoutWait();
                if (msg == null) {
                    logger.debug("No More Tweets");
                    return;
                }
                if (count % 100 == 0) {
                    logger.info(count + ".");
                }

                count++;
                logger.debug(msg);
                socialTeamService.importTweet(jsonParser.parse(msg).getAsJsonObject());
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (JsonSyntaxException e) {
            e.printStackTrace();
        } catch (AppException e) {
            e.printStackTrace();
        }
    }
}
