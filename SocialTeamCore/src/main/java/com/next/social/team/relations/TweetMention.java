package com.next.social.team.relations;

import org.springframework.data.neo4j.annotation.EndNode;
import org.springframework.data.neo4j.annotation.RelationshipEntity;
import org.springframework.data.neo4j.annotation.StartNode;

import com.next.social.team.nodes.Tweet;
import com.next.social.team.nodes.TwitterAccount;

@RelationshipEntity(type = "TWEET_MENTION")
public class TweetMention extends BaseRelationship {

    @StartNode
    private Tweet tweet;
    @EndNode
    private TwitterAccount twitterAccount;

    public TweetMention() {
        super();
    }

    public TweetMention(TwitterAccount twitterAccount, Tweet tweet) {
        super();
        this.twitterAccount = twitterAccount;
        this.tweet = tweet;
    }

    public TwitterAccount getTwitterAccount() {
        return twitterAccount;
    }

    public void setTwitterAccount(TwitterAccount twitterAccount) {
        this.twitterAccount = twitterAccount;
    }

    public Tweet getTweet() {
        return tweet;
    }

    public void setTweet(Tweet tweet) {
        this.tweet = tweet;
    }
	
}
