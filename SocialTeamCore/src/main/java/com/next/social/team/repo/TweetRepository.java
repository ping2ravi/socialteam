package com.next.social.team.repo;

import java.util.List;

import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.GraphRepository;

import com.next.social.team.nodes.HashTag;
import com.next.social.team.nodes.Tweet;
import com.next.social.team.nodes.TwitterAccount;

public interface TweetRepository extends GraphRepository<Tweet> {
	
    @Query("start tweet=node({0}) match (tweet)-[:TWEET_RETWEET]->(tweet) return count(tweet)")
    int getAllRetweetsCountOfTweet(Tweet tweet);

    @Query("start tweet=node({0}) match (tweet)-[:TWEET_RETWEET]->(tweet) return count(tweet)")
    List<Tweet> getAllRetweetsOfTweet(Tweet tweet);

    @Query("start twitterAccount=node({0}) match (twitterAccount)-[:TWEETED|:RETWEETED]->(tweet) return count(tweet)")
    int getAllTimeTweetRetweetCount(TwitterAccount twitterAccount);

    @Query("start twitterAccount=node({0}) match (twitterAccount)-[:TWEETED]->(tweet) return count(tweet)")
    int getAllTimeTweetCount(TwitterAccount twitterAccount);

    @Query("start twitterAccount=node({0}) match (twitterAccount)-[:RETWEETED]->(tweet) return count(tweet)")
    int getAllTimeRetweetCount(TwitterAccount twitterAccount);

    @Query("start hashTag=node({0}) match (hashTag)<-[:TWEET_HASHTAG]-(tweet) return count(tweet)")
    int getAllTimeTweetCountForHashTag(HashTag hashTag);

    @Query("start hashTag=node({0}) match (hashTag)<-[:TWEET_HASHTAG]-(tweet) return tweet")
    List<Tweet> getAllTimeTweetsForHashTag(HashTag hashTag);

    @Query("match tweet where (tweet.__type__='com.next.social.team.nodes.Tweet' or tweet.__type__='Tweet') and tweet.retweet=false and tweet.moderated=false return tweet order by tweet.createdAt Desc limit 100")
    List<Tweet> getTweetsNeedModeration();

    @Query("start twitterAccount=node({0}) match (twitterAccount)-[:TWEETED]->(tweet) return tweet")
    List<Tweet> getUserTweets(TwitterAccount twitterAccount);

}
