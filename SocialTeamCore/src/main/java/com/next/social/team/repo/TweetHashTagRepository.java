package com.next.social.team.repo;

import java.util.List;

import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.GraphRepository;

import com.next.social.team.nodes.HashTag;
import com.next.social.team.nodes.Tweet;
import com.next.social.team.relations.TweetHashTag;

public interface TweetHashTagRepository extends GraphRepository<TweetHashTag> {

    @Query("start tweet=node({0}) , hashTag=node({1}) match (tweet)-[tweetHashTag:TWEET_HASHTAG]->(hashTag) return tweetHashTag")
    TweetHashTag getUserTweethashTags(Tweet tweet, HashTag hashTag);

    @Query("start tweet=node({0}) match (tweet)-[:TWEET_HASHTAG]->(hashTag) return hashTag")
    List<TweetHashTag> getAllHashTagsOfTweet(Tweet tweet);

    @Query("start tweet=node({0}) match (tweet)-[:TWEET_HASHTAG]->(hashTag) return count(hashTag)")
    int getAllHashTagsCountsOfTweet(Tweet tweet);

}
