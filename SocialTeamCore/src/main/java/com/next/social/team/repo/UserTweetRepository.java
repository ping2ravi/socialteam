package com.next.social.team.repo;

import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.GraphRepository;

import com.next.social.team.nodes.Tweet;
import com.next.social.team.nodes.TwitterAccount;
import com.next.social.team.relations.UserTweet;

public interface UserTweetRepository extends GraphRepository<UserTweet> {
	
    @Query("start twitterAccount=node({0}), tweet=node({1}) match (twitterAccount)-[tweeted:TWEETED]->(tweet) return tweeted")
    UserTweet getUserTweet(TwitterAccount twitterAccount, Tweet tweet);

    @Query("start twitterAccount=node({0}), tweet=node({1}) match (twitterAccount)-[tweeted:RETWEETED]->(tweet) return tweeted")
    UserTweet getUserReTweet(TwitterAccount twitterAccount, Tweet tweet);

}
