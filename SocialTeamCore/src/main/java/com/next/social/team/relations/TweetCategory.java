package com.next.social.team.relations;

import org.springframework.data.neo4j.annotation.EndNode;
import org.springframework.data.neo4j.annotation.RelationshipEntity;
import org.springframework.data.neo4j.annotation.StartNode;

import com.next.social.team.nodes.Category;
import com.next.social.team.nodes.Tweet;

@RelationshipEntity(type = "TWEET_CATEGORY")
public class TweetCategory extends BaseRelationship {

    @StartNode
    private Tweet tweet;
    @EndNode
    private Category category;

    public TweetCategory() {
        super();
    }

    public TweetCategory(Tweet tweet, Category category) {
        super();
        this.category = category;
        this.tweet = tweet;
    }


    public Tweet getTweet() {
        return tweet;
    }

    public void setTweet(Tweet tweet) {
        this.tweet = tweet;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }
	
}
