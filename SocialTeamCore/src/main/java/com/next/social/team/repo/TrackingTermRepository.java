package com.next.social.team.repo;

import org.springframework.data.neo4j.repository.GraphRepository;

import com.next.social.team.nodes.TrackingTerm;

public interface TrackingTermRepository extends GraphRepository<TrackingTerm> {
	

}
