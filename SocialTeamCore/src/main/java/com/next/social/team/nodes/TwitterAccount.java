package com.next.social.team.nodes;

import java.util.Date;

import org.springframework.data.neo4j.annotation.Indexed;
import org.springframework.data.neo4j.annotation.NodeEntity;


@NodeEntity
public class TwitterAccount extends BaseNode {

    @Indexed(indexName = "twitterAccountId")
    private String twitterAccountId;

    private String name;

    private String screenName;

    @Indexed(indexName = "screenNameCap")
    private String screenNameCap;

    @Indexed(indexName = "twa_location")
    private String location;

    private String description;

    private boolean secured;

    private boolean verified;

    private int followerCount;

    private int friendsCount;

    private int listedCount;

    private int favouritesCount;

    private int statusCount;


    private Date createdAt;

    private boolean geoEnabled;

    private String lang;

    private String token;

    private String tokenSecret;

    public String getTwitterAccountId() {
        return twitterAccountId;
    }

    public void setTwitterAccountId(String twitterAccountId) {
        this.twitterAccountId = twitterAccountId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getScreenName() {
        return screenName;
    }

    public void setScreenName(String screenName) {
        this.screenName = screenName;
    }

    public String getScreenNameCap() {
        return screenNameCap;
    }

    public void setScreenNameCap(String screenNameCap) {
        this.screenNameCap = screenNameCap;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isSecured() {
        return secured;
    }

    public void setSecured(boolean secured) {
        this.secured = secured;
    }

    public boolean isVerified() {
        return verified;
    }

    public void setVerified(boolean verified) {
        this.verified = verified;
    }

    public int getFollowerCount() {
        return followerCount;
    }

    public void setFollowerCount(int followerCount) {
        this.followerCount = followerCount;
    }

    public int getFriendsCount() {
        return friendsCount;
    }

    public void setFriendsCount(int friendsCount) {
        this.friendsCount = friendsCount;
    }

    public int getListedCount() {
        return listedCount;
    }

    public void setListedCount(int listedCount) {
        this.listedCount = listedCount;
    }

    public int getFavouritesCount() {
        return favouritesCount;
    }

    public void setFavouritesCount(int favouritesCount) {
        this.favouritesCount = favouritesCount;
    }

    public int getStatusCount() {
        return statusCount;
    }

    public void setStatusCount(int statusCount) {
        this.statusCount = statusCount;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAtLong) {
        this.createdAt = createdAtLong;
    }

    public boolean isGeoEnabled() {
        return geoEnabled;
    }

    public void setGeoEnabled(boolean geoEnabled) {
        this.geoEnabled = geoEnabled;
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getTokenSecret() {
        return tokenSecret;
    }

    public void setTokenSecret(String tokenSecret) {
        this.tokenSecret = tokenSecret;
    }

    @Override
    public String toString() {
        return "TwitterAccount [twitterAccountId=" + twitterAccountId + ", name=" + name + ", screenName=" + screenName + ", screenNameCap=" + screenNameCap + ", location=" + location
                + ", description=" + description + ", secured=" + secured + ", verified=" + verified + ", followerCount=" + followerCount + ", friendsCount=" + friendsCount + ", listedCount="
                + listedCount + ", favouritesCount=" + favouritesCount + ", statusCount=" + statusCount + ", createdAtLong=" + createdAt + ", geoEnabled=" + geoEnabled + ", lang=" + lang
                + ", token=" + token + ", tokenSecret=" + tokenSecret + ", id=" + id + "]";
    }
    
}
