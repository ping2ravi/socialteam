package com.next.social.team.dto;

import org.springframework.data.neo4j.annotation.QueryResult;
import org.springframework.data.neo4j.annotation.ResultColumn;

import com.next.social.team.nodes.HashTag;

@QueryResult
public interface HashTagWithCount {

    @ResultColumn("cnt")
    int getCount();

    @ResultColumn("relatedHashTag")
    HashTag getHashTag();

}
