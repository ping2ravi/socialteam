package com.next.social.team.repo;

import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.GraphRepository;

import com.next.social.team.nodes.Tweet;
import com.next.social.team.relations.TweetRetweet;

public interface TweetRetweetRepository extends GraphRepository<TweetRetweet> {
	
    @Query("start originalTweet=node({0}), tweet=node({1}) match (originalTweet)-[retweet:TWEET_RETWEET]->(tweet) return retweet")
    TweetRetweet getTweetRetweet(Tweet originalTweet, Tweet tweet);

}
