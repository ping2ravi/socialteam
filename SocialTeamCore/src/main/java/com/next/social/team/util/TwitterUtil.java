package com.next.social.team.util;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import au.com.bytecode.opencsv.CSVWriter;

import com.next.social.team.exceptions.AppException;
import com.next.social.team.nodes.TrackingTerm;
import com.next.social.team.repo.service.SocialTeamService;
import com.twitter.hbc.ClientBuilder;
import com.twitter.hbc.core.Client;
import com.twitter.hbc.core.Constants;
import com.twitter.hbc.core.Hosts;
import com.twitter.hbc.core.HttpHosts;
import com.twitter.hbc.core.endpoint.StatusesFilterEndpoint;
import com.twitter.hbc.core.event.Event;
import com.twitter.hbc.httpclient.auth.Authentication;
import com.twitter.hbc.httpclient.auth.OAuth1;

@Component
public class TwitterUtil {

    /**
     * Set up your blocking queues: Be sure to size these properly based on
     * expected TPS of your stream
     */
    BlockingQueue<String> msgQueue = new LinkedBlockingQueue<String>(100000);
    BlockingQueue<Event> eventQueue = new LinkedBlockingQueue<Event>(1000);
    
    Client hosebirdClient;

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private SocialTeamService socialTeamService;

    private Set<String> trackingTerms = new TreeSet<>();

    @Scheduled(fixedDelay = 300000)
    public void restartTrackingTerms() {

        try {
            Set<String> latestTerms = getTrackingTermsFromDb();
            if (trackingTerms.equals(latestTerms)) {
                logger.info("No change in tracking Terms");
            } else {
                logger.info("Tracking Terms have changed so will restart Twitter Connection");
                startTracking(latestTerms);
            }
        } catch (AppException e) {
            logger.error("Unable to complete Task restartTrackingTerms", e);
        }

    }

    private Set<String> getTrackingTermsFromDb() throws AppException {
        Set<String> latestTerms = new TreeSet<>();
        List<TrackingTerm> dbTrackingTerms = socialTeamService.getAllTrackingTerms();
        for (TrackingTerm oneTrackingTerm : dbTrackingTerms) {
            latestTerms.add(oneTrackingTerm.getTerm());
        }
        return latestTerms;
    }

    private void startTracking(Set<String> latestTerms) {
        this.trackingTerms = latestTerms;
        if (hosebirdClient != null) {
            hosebirdClient.stop();
        }
        if (trackingTerms.isEmpty()) {
            logger.warn("No Tracking Terms Specified");
            return;
        }
        /**
         * Declare the host you want to connect to, the endpoint, and
         * authentication (basic auth or oauth)
         */
        Hosts hosebirdHosts = new HttpHosts(Constants.STREAM_HOST);
        StatusesFilterEndpoint hosebirdEndpoint = new StatusesFilterEndpoint();
        // Optional: set up some followings and track terms
        List<String> terms = new ArrayList<>(latestTerms);
        // hosebirdEndpoint.followings(followings);
        hosebirdEndpoint.trackTerms(terms);


        // These secrets should be read from a config file
        Authentication hosebirdAuth = new OAuth1("QwAtfFGQ4XyH2qSFX0UOg", "5fmM9fVoDTIgHqKb8OeZ9cZullLdbL0uSrcC3mrTyM", "287659262-oiRPAOumzE9ZKXopLpgWSmhJy8TcV6XESvdQodKx",
                "tTHkHDSvVIcQF0lZ5prAP87wxOd9hQKZPIXoHlEFnWs");

        com.twitter.hbc.core.processor.StringDelimitedProcessor sdp = new com.twitter.hbc.core.processor.StringDelimitedProcessor(msgQueue);

        ClientBuilder builder = new ClientBuilder().name("Hosebird-Client-01").hosts(hosebirdHosts).authentication(hosebirdAuth).endpoint(hosebirdEndpoint).processor(sdp)
                .eventMessageQueue(eventQueue); // optional:
                                                                                                                                            // use
                                                                                                                                            // this
        // if
        // to
        // process
        // client
        // events

        hosebirdClient = builder.build();
        // Attempts to establish a connection.
        hosebirdClient.connect();

        // on a different thread, or multiple different threads....
        // JsonParser jsonParser = new JsonParser();
    }

    @PostConstruct
    public void init() throws InterruptedException, IOException {
        restartTrackingTerms();

    }

    public String getNextTweetMessage() throws InterruptedException {
        if (hosebirdClient.isDone() && msgQueue.isEmpty()) {
            return null;
        }
        String msg = msgQueue.take();
        logger.debug("Message in Queue " + msgQueue.size());
        return msg;
    }

    public String getNextTweetMessageWithoutWait() throws InterruptedException {
        if (hosebirdClient.isDone() && msgQueue.isEmpty()) {
            return null;
        }
        String msg = msgQueue.poll();
        logger.debug("Message in Queue " + msgQueue.size());
        return msg;
    }

    private static CSVWriter getCsvFile(int count, String dateTimeString) throws IOException {
        String countStr;
        if (count < 10) {
            countStr = "0" + count;
        } else {
            countStr = String.valueOf(count);
        }
        String csvFileNameAndPath = "/Users/ravisharma/ravi/aap/tweets_" + dateTimeString + "_" + countStr + ".csv";
        File csvFile = new File(csvFileNameAndPath);
        CSVWriter csvWriter = new CSVWriter(new FileWriter(csvFile, true));
        String[] headers = { "User", "TweetId", "Tweet", "TweetUrl", "Retweet Count", "Favourite Count" };
        csvWriter.writeNext(headers);
        return csvWriter;
    }
}
