package com.next.social.team.repo.service;

import java.util.List;

import com.google.gson.JsonObject;
import com.next.social.team.dto.HashTagWithCount;
import com.next.social.team.dto.TwitterAccountWithCount;
import com.next.social.team.exceptions.AppException;
import com.next.social.team.nodes.Category;
import com.next.social.team.nodes.HashTag;
import com.next.social.team.nodes.TrackingTerm;
import com.next.social.team.nodes.Tweet;
import com.next.social.team.nodes.TwitterAccount;

public interface SocialTeamService {

    Tweet importTweet(JsonObject tweetJson) throws AppException;

    List<TrackingTerm> getAllTrackingTerms() throws AppException;

    TrackingTerm saveTrackingTerm(TrackingTerm trackingTerm) throws AppException;

    HashTag getHashTagById(Long id) throws AppException;

    HashTag getHashTagById(String tag) throws AppException;

    List<HashTagWithCount> getRelatedHashTags(Long id) throws AppException;

    List<TwitterAccountWithCount> getTwitterAccountForHashTag(String hashTag) throws AppException;

    List<Category> getAllCategories() throws AppException;

    Category saveCategory(Category category) throws AppException;

    List<Tweet> getTweetsNeedModeration() throws AppException;
    
    Tweet updateTweetCategory(Tweet tweet, Category category) throws AppException;

    TwitterAccount updateTwitterAccountCategory(String screenName, Category category) throws AppException;

    List<Tweet> updateUserAccountTweetCategory(TwitterAccount twitterAccount, Category category) throws AppException;
    
    Tweet updateTweetsRetweetCategory(Tweet tweet, Category category) throws AppException;

}
