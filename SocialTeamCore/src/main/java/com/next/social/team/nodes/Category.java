package com.next.social.team.nodes;

import org.springframework.data.neo4j.annotation.Indexed;
import org.springframework.data.neo4j.annotation.NodeEntity;


@NodeEntity
public class Category extends BaseNode {

    private String name;

    @Indexed(indexName = "categoryNameCap")
    private String nameCap;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNameCap() {
        return nameCap;
    }

    public void setNameCap(String nameCap) {
        this.nameCap = nameCap;
    }

}
