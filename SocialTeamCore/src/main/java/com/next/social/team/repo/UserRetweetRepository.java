package com.next.social.team.repo;

import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.GraphRepository;

import com.next.social.team.nodes.Tweet;
import com.next.social.team.nodes.TwitterAccount;
import com.next.social.team.relations.UserRetweet;

public interface UserRetweetRepository extends GraphRepository<UserRetweet> {
	
    @Query("start twitterAccount=node({0}), tweet=node({1}) match (twitterAccount)-[retweet:RETWEETED]->(tweet) return retweet")
    UserRetweet getUserRetweet(TwitterAccount twitterAccount, Tweet tweet);
}
