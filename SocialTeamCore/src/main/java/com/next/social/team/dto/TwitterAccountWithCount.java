package com.next.social.team.dto;

import org.springframework.data.neo4j.annotation.QueryResult;
import org.springframework.data.neo4j.annotation.ResultColumn;

import com.next.social.team.nodes.TwitterAccount;

@QueryResult
public interface TwitterAccountWithCount {

    @ResultColumn("cnt")
    int getCount();

    @ResultColumn("twitterAccount")
    TwitterAccount getTwitterAccount();

}
