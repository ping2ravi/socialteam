package com.next.social.team.nodes;

import java.util.Date;

import org.springframework.data.neo4j.annotation.Indexed;
import org.springframework.data.neo4j.annotation.NodeEntity;


@NodeEntity
public class Tweet extends BaseNode {

    @Indexed(indexName = "tweetId")
    private String tweetId;

    private String text;

    private Date createdAt;

    private String replyToTweetId;

    private String replyToUserId;

    private String replyToScreenName;

    private boolean replyToTweet;

    private boolean replyToUser;

    private boolean retweet;

    private int retweetCount;

    private int favouriteCount;

    private String twitterJson;

    private long timeStampMs;

    private boolean moderated;

    private String screenName;

    private String retweetScreenName;

    public String getTweetId() {
        return tweetId;
    }

    public void setTweetId(String tweetId) {
        this.tweetId = tweetId;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public String getReplyToTweetId() {
        return replyToTweetId;
    }

    public void setReplyToTweetId(String replyToTweetId) {
        this.replyToTweetId = replyToTweetId;
    }

    public String getReplyToUserId() {
        return replyToUserId;
    }

    public void setReplyToUserId(String replyToUserId) {
        this.replyToUserId = replyToUserId;
    }

    public String getReplyToScreenName() {
        return replyToScreenName;
    }

    public void setReplyToScreenName(String replyToScreenName) {
        this.replyToScreenName = replyToScreenName;
    }

    public boolean isReplyToTweet() {
        return replyToTweet;
    }

    public void setReplyToTweet(boolean replyToTweet) {
        this.replyToTweet = replyToTweet;
    }

    public boolean isReplyToUser() {
        return replyToUser;
    }

    public void setReplyToUser(boolean replyToUser) {
        this.replyToUser = replyToUser;
    }

    public boolean isRetweet() {
        return retweet;
    }

    public void setRetweet(boolean retweet) {
        this.retweet = retweet;
    }

    public int getRetweetCount() {
        return retweetCount;
    }

    public void setRetweetCount(int retweetCount) {
        this.retweetCount = retweetCount;
    }

    public int getFavouriteCount() {
        return favouriteCount;
    }

    public void setFavouriteCount(int favouriteCount) {
        this.favouriteCount = favouriteCount;
    }

    public String getTwitterJson() {
        return twitterJson;
    }

    public void setTwitterJson(String twitterJson) {
        this.twitterJson = twitterJson;
    }

    public long getTimeStampMs() {
        return timeStampMs;
    }

    public void setTimeStampMs(long timeStampMs) {
        this.timeStampMs = timeStampMs;
    }

    public boolean isModerated() {
        return moderated;
    }

    public void setModerated(boolean moderated) {
        this.moderated = moderated;
    }

    public String getScreenName() {
        return screenName;
    }

    public void setScreenName(String screenName) {
        this.screenName = screenName;
    }

    public String getRetweetScreenName() {
        return retweetScreenName;
    }

    public void setRetweetScreenName(String retweetScreenName) {
        this.retweetScreenName = retweetScreenName;
    }

}
