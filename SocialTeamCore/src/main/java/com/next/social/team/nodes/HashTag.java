package com.next.social.team.nodes;

import org.springframework.data.neo4j.annotation.Indexed;
import org.springframework.data.neo4j.annotation.NodeEntity;


@NodeEntity
public class HashTag extends BaseNode {

    @Indexed(indexName = "tag")
    private String tag;

    @Indexed(indexName = "tagCap")
    private String tagCap;

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getTagCap() {
        return tagCap;
    }

    public void setTagCap(String tagCap) {
        this.tagCap = tagCap;
    }

    @Override
    public String toString() {
        return "HashTag [tag=" + tag + ", id=" + id + "]";
    }

}
