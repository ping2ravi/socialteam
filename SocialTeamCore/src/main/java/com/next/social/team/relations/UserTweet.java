package com.next.social.team.relations;

import org.springframework.data.neo4j.annotation.EndNode;
import org.springframework.data.neo4j.annotation.RelationshipEntity;
import org.springframework.data.neo4j.annotation.RelationshipType;
import org.springframework.data.neo4j.annotation.StartNode;

import com.next.social.team.nodes.Tweet;
import com.next.social.team.nodes.TwitterAccount;

@RelationshipEntity
public class UserTweet extends BaseRelationship {

    @StartNode
    private TwitterAccount twitterAccount;
    @EndNode
    private Tweet tweet;
    @RelationshipType
    private String label;

    public UserTweet() {
        super();
    }

    public UserTweet(TwitterAccount twitterAccount, Tweet tweet, String label) {
        super();
        this.twitterAccount = twitterAccount;
        this.tweet = tweet;
        this.label = label;
    }

    public TwitterAccount getTwitterAccount() {
        return twitterAccount;
    }

    public void setTwitterAccount(TwitterAccount twitterAccount) {
        this.twitterAccount = twitterAccount;
    }

    public Tweet getTweet() {
        return tweet;
    }

    public void setTweet(Tweet tweet) {
        this.tweet = tweet;
    }
	
}
