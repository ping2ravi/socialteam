package com.next.social.team.repo;

import java.util.List;

import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.conversion.EndResult;
import org.springframework.data.neo4j.repository.GraphRepository;

import com.next.social.team.dto.TwitterAccountWithCount;
import com.next.social.team.nodes.HashTag;
import com.next.social.team.nodes.Tweet;
import com.next.social.team.nodes.TwitterAccount;

public interface TwitterAccountRepository extends GraphRepository<TwitterAccount> {
	
    @Query("start tweet=node({0}) match (tweet)-[:TWEET_RETWEET]->(retweet)-[:RETWEETED]-(twitterAccount) return twitterAccount")
    List<TwitterAccount> getAllTwitterAccountWhoRetweeted(Tweet tweet);

    @Query("start hashTag=node({0}) match (hashTag)<-[:TWEET_HASHTAG]-(tweet)-[:TWEETED|:RETWEETED]-(twitterAccount)  return twitterAccount,count(tweet) as cnt order by cnt desc limit 100")
    EndResult<TwitterAccountWithCount> getTweeterAccountTweetedRetweetedHashTag(HashTag hashTag);
    

}
