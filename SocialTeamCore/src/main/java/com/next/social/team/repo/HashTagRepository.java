package com.next.social.team.repo;

import java.util.List;

import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.conversion.EndResult;
import org.springframework.data.neo4j.repository.GraphRepository;

import com.next.social.team.dto.HashTagWithCount;
import com.next.social.team.nodes.HashTag;

public interface HashTagRepository extends GraphRepository<HashTag> {

    @Query("start hashTag=node({0}) match (hashTag)<-[:TWEET_HASHTAG]-(tweet)-[:TWEET_HASHTAG]->(hashtag2)  return hashtag2 as relatedHashTag, count(tweet) as cnt order by cnt desc limit 1000")
    EndResult<HashTagWithCount> getRelatedHashTagWithCount(Long hashTagId);

    @Query("start hashTag=node({0}) match (hashTag)<-[:TWEET_HASHTAG]-(tweet)-[:TWEET_HASHTAG]->(hashtag2)  return distinct hashtag2 as relatedHashTag limit 50")
    List<HashTag> getRelatedHashTag(Long hashTag);
}
