package com.next.social.team.relations;

import org.springframework.data.neo4j.annotation.EndNode;
import org.springframework.data.neo4j.annotation.RelationshipEntity;
import org.springframework.data.neo4j.annotation.StartNode;

import com.next.social.team.nodes.HashTag;
import com.next.social.team.nodes.Tweet;

@RelationshipEntity(type = "TWEET_HASHTAG")
public class TweetHashTag extends BaseRelationship {

    @StartNode
    private Tweet tweet;
    @EndNode
    private HashTag hashTag;

    public TweetHashTag() {
        super();
    }

    public TweetHashTag(Tweet tweet, HashTag hashTag) {
        super();
        this.tweet = tweet;
        this.hashTag = hashTag;
    }

    public Tweet getTweet() {
        return tweet;
    }

    public void setTweet(Tweet tweet) {
        this.tweet = tweet;
    }

    public HashTag getHashTag() {
        return hashTag;
    }

    public void setHashTag(HashTag hashTag) {
        this.hashTag = hashTag;
    }

    @Override
    public String toString() {
        return "TweetHashTag [tweet=" + tweet + ", hashTag=" + hashTag + "]";
    }
	
}
