package com.next.social.team.relations;

import org.springframework.data.neo4j.annotation.EndNode;
import org.springframework.data.neo4j.annotation.RelationshipEntity;
import org.springframework.data.neo4j.annotation.StartNode;

import com.next.social.team.nodes.Category;
import com.next.social.team.nodes.TwitterAccount;

@RelationshipEntity(type = "TWITTER_ACCOUNT_CATEGORY")
public class TwitterAccountCategory extends BaseRelationship {

    @StartNode
    private TwitterAccount twitterAccount;;
    @EndNode
    private Category category;

    public TwitterAccountCategory() {
        super();
    }

    public TwitterAccountCategory(TwitterAccount twitterAccount, Category category) {
        super();
        this.category = category;
        this.twitterAccount = twitterAccount;
    }



    public TwitterAccount getTwitterAccount() {
        return twitterAccount;
    }

    public void setTwitterAccount(TwitterAccount twitterAccount) {
        this.twitterAccount = twitterAccount;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }
	
}
