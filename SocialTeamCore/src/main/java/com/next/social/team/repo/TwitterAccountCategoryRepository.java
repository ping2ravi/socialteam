package com.next.social.team.repo;

import java.util.List;

import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.GraphRepository;

import com.next.social.team.nodes.TwitterAccount;
import com.next.social.team.relations.TwitterAccountCategory;

public interface TwitterAccountCategoryRepository extends GraphRepository<TwitterAccountCategory> {

    @Query("start twitterAccount=node({0}) match (twitterAccount)-[twitterAccountCatergory:TWITTER_ACCOUNT_CATEGORY]->(category) return twitterAccountCatergory")
    List<TwitterAccountCategory> getTwitterAccountCategoryByTwitterAccount(TwitterAccount twitterAccount);

}
