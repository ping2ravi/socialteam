package com.next.social.team.repo.service;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.neo4j.conversion.EndResult;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.next.social.team.dto.HashTagWithCount;
import com.next.social.team.dto.TwitterAccountWithCount;
import com.next.social.team.exceptions.AppException;
import com.next.social.team.nodes.Category;
import com.next.social.team.nodes.HashTag;
import com.next.social.team.nodes.TrackingTerm;
import com.next.social.team.nodes.Tweet;
import com.next.social.team.nodes.TwitterAccount;
import com.next.social.team.relations.TweetCategory;
import com.next.social.team.relations.TweetHashTag;
import com.next.social.team.relations.TweetMention;
import com.next.social.team.relations.TweetRetweet;
import com.next.social.team.relations.TwitterAccountCategory;
import com.next.social.team.relations.UserTweet;
import com.next.social.team.repo.CategoryRepository;
import com.next.social.team.repo.HashTagRepository;
import com.next.social.team.repo.TrackingTermRepository;
import com.next.social.team.repo.TweetCategoryRepository;
import com.next.social.team.repo.TweetHashTagRepository;
import com.next.social.team.repo.TweetMentionRepository;
import com.next.social.team.repo.TweetRepository;
import com.next.social.team.repo.TweetRetweetRepository;
import com.next.social.team.repo.TwitterAccountCategoryRepository;
import com.next.social.team.repo.TwitterAccountRepository;
import com.next.social.team.repo.UserRetweetRepository;
import com.next.social.team.repo.UserTweetRepository;

@Component
@Transactional
public class SocialTeamServiceImpl implements SocialTeamService, Serializable {

    private static final long serialVersionUID = 1L;
    private static final String RETWEETED_STATUS_FIELD_NAME = "retweeted_status";
    private static final ThreadLocal<DateFormat> dateFormatThreadLocal = new ThreadLocal<DateFormat>() {
        @Override
        protected DateFormat initialValue() {
            return new SimpleDateFormat("EEE MMM dd HH:mm:ss z yyyy");
        }
    };
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private TweetRepository tweetRepository;
    @Autowired
    private TwitterAccountRepository twitterAccountRepository;
    @Autowired
    private HashTagRepository hashTagRepository;
    @Autowired
    private UserRetweetRepository userRetweetRepository;
    @Autowired
    private UserTweetRepository userTweetRepository;
    @Autowired
    private TweetMentionRepository tweetMentionRepository;
    @Autowired
    private TweetHashTagRepository tweetHashTagRepository;
    @Autowired
    private TweetRetweetRepository tweetRetweetRepository;
    @Autowired
    private TrackingTermRepository trackingTermRepository;
    @Autowired
    private CategoryRepository categoryRepository;
    @Autowired
    private TweetCategoryRepository tweetCategoryRepository;
    @Autowired
    private TwitterAccountCategoryRepository twitterAccountCategoryRepository;

    @Override
    public Tweet importTweet(JsonObject tweetJson) throws AppException {
        long startTime = System.currentTimeMillis();
        Tweet tweet = null;
        try {
            boolean retweet = isRetweet(tweetJson);
            logger.debug("retweet= {}", retweet);

            Tweet originalTweet = null;
            if (retweet) {
                // First save retweet status and if its already saved then
                // update
                // retweet count

                JsonObject retweeJsonObject = tweetJson.get(RETWEETED_STATUS_FIELD_NAME).getAsJsonObject();
                originalTweet = processTweetJson(retweeJsonObject, null);

            }
            tweet = processTweetJson(tweetJson, originalTweet);
            if (originalTweet != null) {
                // Add extra relation of retweet
                saveTweetRetweet(originalTweet, tweet);
            }

        } catch (Exception ex) {
            throw new AppException(ex);
        } finally {
            long endTime = System.currentTimeMillis();
            logger.debug("Time to Import : {} ms", endTime - startTime);
        }
        return tweet;
    }


    private Tweet processTweetJson(JsonObject tweetJson, Tweet originalTweet) throws ParseException {
        TwitterAccount twitterAccount = saveTwitterAccount(tweetJson.get("user").getAsJsonObject());
        boolean retweet = (originalTweet != null);
        Tweet tweet = saveTweet(tweetJson, twitterAccount, originalTweet);
        List<HashTag> hashTags = saveHashtags(tweetJson);
        // Now Create Relations
        UserTweet userTweet = saveUserTweetRelation(twitterAccount, tweet, retweet);
        saveUserMentions(tweetJson, tweet);
        saveTweetHashTags(tweet, hashTags);
        return tweet;
    }

    private TweetRetweet saveTweetRetweet(Tweet originalTweet, Tweet tweet) {
        TweetRetweet tweetRetweet = tweetRetweetRepository.getTweetRetweet(originalTweet, tweet);
        if (tweetRetweet == null) {
            tweetRetweet = new TweetRetweet(originalTweet, tweet);
            tweetRetweet = tweetRetweetRepository.save(tweetRetweet);
        }
        return tweetRetweet;
    }

    private UserTweet saveUserTweetRelation(TwitterAccount twitterAccount, Tweet tweet, boolean retweet) {
        UserTweet userTweet;
            if(retweet){
            userTweet = userTweetRepository.getUserReTweet(twitterAccount, tweet);
                if (userTweet == null) {
                    userTweet = new UserTweet(twitterAccount, tweet, "RETWEETED");
                    userTweet = userTweetRepository.save(userTweet);
                }
                    
            }else{
            userTweet = userTweetRepository.getUserTweet(twitterAccount, tweet);
                if (userTweet == null) {
                    userTweet = new UserTweet(twitterAccount, tweet, "TWEETED");
                    userTweet = userTweetRepository.save(userTweet);
                }
            }
            
        return userTweet;
    }

    private List<HashTag> saveHashtags(JsonObject tweetJson){
        List<HashTag> hashTags = new ArrayList<HashTag>();
        if(tweetJson.get("entities") == null){
            return hashTags;
        }
        if (tweetJson.get("entities").getAsJsonObject().get("hashtags") == null) {
            return hashTags;
        }
        JsonArray jsonArray = tweetJson.get("entities").getAsJsonObject().get("hashtags").getAsJsonArray();
        for (int i = 0; i < jsonArray.size(); i++) {
            HashTag hashTag = saveHashTag(jsonArray.get(i).getAsJsonObject());
            logger.debug("Saved hashTag : {}", hashTag);
            hashTags.add(hashTag);
        }
        return hashTags;
    }

    private HashTag saveHashTag(JsonObject jsonObject) {
        String tag = getString(jsonObject, "text", null);
        HashTag hashTag = hashTagRepository.findByPropertyValue("tagCap", tag.toUpperCase());
        if (hashTag == null) {
            hashTag = new HashTag();
            hashTag.setTag(tag);
            hashTag.setTagCap(tag.toUpperCase());
            hashTag = hashTagRepository.save(hashTag);
        }
        return hashTag;
    }

    private List<TweetMention> saveUserMentions(JsonObject tweetJson, Tweet tweet) throws ParseException {
        logger.debug("Creating User mentions");
        List<TweetMention> tweetMentions = new ArrayList<TweetMention>();
        if (tweetJson.get("entities") == null) {
            logger.debug("No User mentions");
            return tweetMentions;
        }
        if (tweetJson.get("entities").getAsJsonObject().get("user_mentions") == null) {
            return tweetMentions;
        }

        JsonArray jsonArray = tweetJson.get("entities").getAsJsonObject().get("user_mentions").getAsJsonArray();
        for (int i = 0; i < jsonArray.size(); i++) {
            logger.debug("Creating Tweet mention");
            TwitterAccount twitterAccount = saveTwitterAccount(jsonArray.get(i).getAsJsonObject());
            logger.debug("twitterAccount = ", twitterAccount);
            TweetMention tweetMention = saveTweetMention(twitterAccount, tweet);
            tweetMentions.add(tweetMention);
        }
        return tweetMentions;
    }

    private TweetMention saveTweetMention(TwitterAccount twitterAccount, Tweet tweet) {
        TweetMention tweetMention = tweetMentionRepository.getUserTweetMention(twitterAccount, tweet);
        if (tweetMention == null) {
            tweetMention = new TweetMention(twitterAccount, tweet);
            tweetMention = tweetMentionRepository.save(tweetMention);
        }
        return tweetMention;
    }

    private List<TweetHashTag> saveTweetHashTags(Tweet tweet, List<HashTag> hashTags) throws ParseException {
        List<TweetHashTag> tweetHashTags = new ArrayList<TweetHashTag>();
        if (hashTags == null || hashTags.isEmpty()) {
            return tweetHashTags;
        }
        logger.debug("hashTags : {}", hashTags);
        for (HashTag oneHashTag : hashTags) {
            TweetHashTag tweetHashTag = saveTweethashTag(tweet, oneHashTag);
            logger.debug("Created tweetHashTag for : {} , {}", oneHashTag, tweetHashTag);
            tweetHashTags.add(tweetHashTag);
        }
        return tweetHashTags;
    }

    private TweetHashTag saveTweethashTag(Tweet tweet, HashTag hashTag) {
        logger.debug("creating tweetHashTag for : {} and {}", hashTag.getId(), tweet.getId());
        TweetHashTag tweetHashTag = tweetHashTagRepository.getUserTweethashTags(tweet, hashTag);
        if (tweetHashTag == null) {
            logger.debug("No TweetHashTag found so creating new one");
            tweetHashTag = new TweetHashTag(tweet, hashTag);
            tweetHashTag = tweetHashTagRepository.save(tweetHashTag);
        } else {
            logger.debug("Found tweetHashTag : {} and {}", tweetHashTag.getHashTag().getId(), tweetHashTag.getTweet().getId());
        }
        return tweetHashTag;
    }


    private Tweet saveTweet(JsonObject tweetJson, TwitterAccount twitterAccount, Tweet originalTweet) {
        DateFormat df = dateFormatThreadLocal.get();
        boolean retweet = (originalTweet != null);
        String tweetId = getString(tweetJson, "id_str", null);
        Tweet tweet = tweetRepository.findByPropertyValue("tweetId", tweetId);
        if (tweet == null) {
            tweet = new Tweet();
            tweet.setTweetId(tweetId);
            tweet.setDateCreated(new Date());
            tweet.setCreatedAt(getDate(df, tweetJson, "created_at", null));
            tweet.setTimeStampMs(getLong(tweetJson, "timestamp_ms", 0L));
            tweet.setRetweet(retweet);
            tweet.setModerated(false);
        }
        tweet.setDateModified(new Date());
        tweet.setFavouriteCount(getMaxInt(tweetJson, "favorite_count", tweet.getFavouriteCount()));
        tweet.setReplyToScreenName(getString(tweetJson, "in_reply_to_screen_name", null));
        tweet.setReplyToTweetId(getString(tweetJson, "in_reply_to_status_id_str", null));
        tweet.setReplyToUserId(getString(tweetJson, "in_reply_to_user_id_str", null));
        tweet.setText(getString(tweetJson, "text", null));
        tweet.setScreenName(twitterAccount.getScreenName());
        tweet.setRetweetCount(getMaxInt(tweetJson, "retweet_count", tweet.getRetweetCount()));

        // tweet.setTwitterJson(tweetJson.toString());

        tweet = tweetRepository.save(tweet);

        Category category = null;
        if (originalTweet != null) {
            tweet.setRetweetScreenName(originalTweet.getScreenName());
            List<TweetCategory> tweetCategories = tweetCategoryRepository.getTweetCategoryByTweet(originalTweet);
            if (tweetCategories != null && !tweetCategories.isEmpty()) {
                category = tweetCategories.get(0).getCategory();
            }

        }
        if (category == null) {
            // get category from URL
            List<TwitterAccountCategory> twitterAccountCategories = twitterAccountCategoryRepository.getTwitterAccountCategoryByTwitterAccount(twitterAccount);
            if (twitterAccountCategories != null && !twitterAccountCategories.isEmpty()) {
                category = twitterAccountCategories.get(0).getCategory();
            }
        }

        if (category != null) {
            TweetCategory tweetCategory = new TweetCategory(tweet, category);
            tweetCategory = tweetCategoryRepository.save(tweetCategory);
        }
        return tweet;
    }

    private TwitterAccount saveTwitterAccount(JsonObject userTweetJson) throws ParseException {

        DateFormat df = dateFormatThreadLocal.get();
        String twitterAccountId = getString(userTweetJson, "id_str", null);
        TwitterAccount twitterAccount = twitterAccountRepository.findByPropertyValue("twitterAccountId", twitterAccountId);
        if(twitterAccount == null){
            twitterAccount = new TwitterAccount();
            twitterAccount.setCreatedAt(getDate(df, userTweetJson, "created_at", null));
            twitterAccount.setDateCreated(new Date());
            twitterAccount.setTwitterAccountId(twitterAccountId);
        }
        twitterAccount.setDateModified(new Date());
        twitterAccount.setDescription(getString(userTweetJson, "description", null));
        twitterAccount.setFavouritesCount(getMaxInt(userTweetJson, "favourites_count", twitterAccount.getFavouritesCount()));
        twitterAccount.setFollowerCount(getMaxInt(userTweetJson, "followers_count", twitterAccount.getFollowerCount()));
        twitterAccount.setFriendsCount(getMaxInt(userTweetJson, "friends_count", twitterAccount.getFriendsCount()));
        twitterAccount.setGeoEnabled(getBoolean(userTweetJson, "geo_enabled", false));
        twitterAccount.setLang(getString(userTweetJson, "lang", null));
        twitterAccount.setListedCount(getMaxInt(userTweetJson, "listed_count", twitterAccount.getListedCount()));
        twitterAccount.setLocation(getString(userTweetJson, "location", null));
        twitterAccount.setName(getString(userTweetJson, "name", null));
        twitterAccount.setScreenName(getString(userTweetJson, "screen_name", null));
        twitterAccount.setScreenNameCap(twitterAccount.getScreenName().toUpperCase());
        twitterAccount.setSecured(getBoolean(userTweetJson, "protected", false));
        twitterAccount.setVerified(getBoolean(userTweetJson, "verified", false));
        twitterAccount.setStatusCount(getMaxInt(userTweetJson, "statuses_count", twitterAccount.getStatusCount()));
        twitterAccount.setVerified(getBoolean(userTweetJson, "verified", false));
        twitterAccount = twitterAccountRepository.save(twitterAccount);
        return twitterAccount;
    }

    private String getString(JsonObject jsonObject, String fieldName, String defaultValue) {
        try{
            return jsonObject.get(fieldName).getAsString();
        } catch(Exception ex){
        }
        return defaultValue;
    }

    private int getInt(JsonObject jsonObject, String fieldName, int defaultValue) {
        try {
            return jsonObject.get(fieldName).getAsInt();
        } catch (Exception ex) {
        }
        return defaultValue;
    }

    private int getMaxInt(JsonObject jsonObject, String fieldName, int defaultValue) {
        try {
            int value = jsonObject.get(fieldName).getAsInt();
            if (value > defaultValue) {
                return value;
            }
        } catch (Exception ex) {
        }
        return defaultValue;
    }

    private boolean getBoolean(JsonObject jsonObject, String fieldName, boolean defaultValue) {
        try {
            return jsonObject.get(fieldName).getAsBoolean();
        } catch (Exception ex) {
        }
        return defaultValue;
    }

    private long getLong(JsonObject jsonObject, String fieldName, long defaultValue) {
        try {
            return jsonObject.get(fieldName).getAsLong();
        } catch (Exception ex) {
        }
        return defaultValue;
    }

    private Date getDate(DateFormat df, JsonObject jsonObject, String fieldName, Date defaultValue) {
        try{
            return df.parse(jsonObject.get(fieldName).getAsString());
        } catch(Exception ex){
        }
        return defaultValue;
    }

    private boolean isRetweet(JsonObject tweetJson) {
        if (tweetJson.get(RETWEETED_STATUS_FIELD_NAME) == null) {
            return false;
        }
        return true;
    }

    @Override
    public List<TrackingTerm> getAllTrackingTerms() throws AppException {
        EndResult<TrackingTerm> dbTrackingTerms = trackingTermRepository.findAll();
        List<TrackingTerm> returnResults = new ArrayList<>();
        for (TrackingTerm oneTrackingTerms : dbTrackingTerms) {
            returnResults.add(oneTrackingTerms);
        }
        return returnResults;
    }

    @Override
    public TrackingTerm saveTrackingTerm(TrackingTerm trackingTerm) throws AppException {
        return trackingTermRepository.save(trackingTerm);
    }

    @Override
    public HashTag getHashTagById(Long id) throws AppException {
        return hashTagRepository.findOne(id);
    }

    @Override
    public HashTag getHashTagById(String tag) throws AppException {
        return hashTagRepository.findByPropertyValue("tagCap", tag.toUpperCase());
    }

    @Override
    public List<HashTagWithCount> getRelatedHashTags(Long hashTagId) throws AppException {
        EndResult<HashTagWithCount> result = hashTagRepository.getRelatedHashTagWithCount(hashTagId);
        List<HashTagWithCount> list = new ArrayList<>();
        for (HashTagWithCount oneEntry : result) {
            list.add(oneEntry);
        }
        return list;
    }

    @Override
    public List<TwitterAccountWithCount> getTwitterAccountForHashTag(String tag) throws AppException {
        HashTag hashTag = hashTagRepository.findByPropertyValue("tagCap", tag.toUpperCase());
        if (hashTag == null) {
            return null;
        }
        EndResult<TwitterAccountWithCount> result = twitterAccountRepository.getTweeterAccountTweetedRetweetedHashTag(hashTag);
        List<TwitterAccountWithCount> list = new ArrayList<>();
        for (TwitterAccountWithCount oneEntry : result) {
            list.add(oneEntry);
        }
        return list;
    }

    @Override
    public List<Category> getAllCategories() throws AppException {
        EndResult<Category> dbTrackingTerms = categoryRepository.findAll();
        List<Category> returnResults = new ArrayList<>();
        for (Category oneTrackingTerms : dbTrackingTerms) {
            returnResults.add(oneTrackingTerms);
        }
        return returnResults;
    }

    @Override
    public Category saveCategory(Category category) throws AppException {
        return categoryRepository.save(category);
    }

    @Override
    public List<Tweet> getTweetsNeedModeration() throws AppException {
        return tweetRepository.getTweetsNeedModeration();
    }

    @Override
    public Tweet updateTweetCategory(Tweet tweet, Category category) throws AppException {
        logger.info("Updaing Category to {} for tweet {}", category.getName(), tweet.getText());
        TweetCategory tweetCategory = new TweetCategory(tweet, category);
        tweetCategory = tweetCategoryRepository.save(tweetCategory);
        tweet.setModerated(true);
        tweet = tweetRepository.save(tweet);
        return tweet;
    }

    @Override
    public TwitterAccount updateTwitterAccountCategory(String screenName, Category category) throws AppException {
        TwitterAccount twitterAccount = twitterAccountRepository.findByPropertyValue("screenNameCap", screenName.toUpperCase());
        if (twitterAccount == null) {
            throw new AppException("No Twiiter Account foubd : " + screenName);
        }
        TwitterAccountCategory twitterAccountCategory = new TwitterAccountCategory(twitterAccount, category);
        twitterAccountCategory = twitterAccountCategoryRepository.save(twitterAccountCategory);
        return twitterAccount;
    }

    @Override
    public List<Tweet> updateUserAccountTweetCategory(TwitterAccount twitterAccount, Category category) throws AppException {
        List<Tweet> tweets = tweetRepository.getUserTweets(twitterAccount);
        for (Tweet oneTweet : tweets) {
            List<TweetCategory> tweetCategories = tweetCategoryRepository.getTweetCategoryByTweet(oneTweet);
            if (tweetCategories != null && !tweetCategories.isEmpty()) {
                tweetCategoryRepository.delete(tweetCategories);
            }
            logger.info("Updating Tweet {} category to {}", oneTweet.getText(), category.getName());
            TweetCategory tweetCategory = new TweetCategory(oneTweet, category);
            tweetCategory = tweetCategoryRepository.save(tweetCategory);
            oneTweet.setModerated(true);
            oneTweet = tweetRepository.save(oneTweet);
        }
        return tweets;
    }

    @Override
    public Tweet updateTweetsRetweetCategory(Tweet tweet, Category category) throws AppException {
        logger.info("Updating Main Tweet's Retweet {} category to {}", tweet.getText(), category.getName());
        List<Tweet> tweets = tweetRepository.getAllRetweetsOfTweet(tweet);
        for (Tweet oneTweet : tweets) {
            List<TweetCategory> tweetCategories = tweetCategoryRepository.getTweetCategoryByTweet(oneTweet);
            if (tweetCategories != null && !tweetCategories.isEmpty()) {
                tweetCategoryRepository.delete(tweetCategories);
            }
            logger.info("Updating Tweet {} category to {}", oneTweet.getText(), category.getName());
            TweetCategory tweetCategory = new TweetCategory(oneTweet, category);
            tweetCategory = tweetCategoryRepository.save(tweetCategory);
            oneTweet.setModerated(true);
            oneTweet = tweetRepository.save(oneTweet);
        }
        return tweet;
    }

}
