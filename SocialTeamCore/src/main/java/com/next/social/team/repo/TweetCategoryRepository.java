package com.next.social.team.repo;

import java.util.List;

import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.GraphRepository;

import com.next.social.team.nodes.Tweet;
import com.next.social.team.relations.TweetCategory;

public interface TweetCategoryRepository extends GraphRepository<TweetCategory> {

    @Query("start tweet=node({0}) match (tweet)-[tweetCatergory:TWEET_CATEGORY]->(category) return tweetCatergory")
    List<TweetCategory> getTweetCategoryByTweet(Tweet tweet);

}
