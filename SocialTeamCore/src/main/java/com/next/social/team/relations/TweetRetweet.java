package com.next.social.team.relations;

import org.springframework.data.neo4j.annotation.EndNode;
import org.springframework.data.neo4j.annotation.RelationshipEntity;
import org.springframework.data.neo4j.annotation.StartNode;

import com.next.social.team.nodes.Tweet;

@RelationshipEntity(type = "TWEET_RETWEET")
public class TweetRetweet extends BaseRelationship {

    @StartNode
    private Tweet originalTweet;
    @EndNode
    private Tweet retweet;

    public TweetRetweet() {
        super();
    }

    public TweetRetweet(Tweet originalTweet, Tweet retweet) {
        super();
        this.originalTweet = originalTweet;
        this.retweet = retweet;
    }

    public Tweet getOriginalTweet() {
        return originalTweet;
    }

    public void setOriginalTweet(Tweet originalTweet) {
        this.originalTweet = originalTweet;
    }

    public Tweet getRetweet() {
        return retweet;
    }

    public void setRetweet(Tweet retweet) {
        this.retweet = retweet;
    }
	
}
