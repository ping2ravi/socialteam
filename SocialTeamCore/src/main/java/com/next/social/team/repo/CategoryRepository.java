package com.next.social.team.repo;

import org.springframework.data.neo4j.repository.GraphRepository;

import com.next.social.team.nodes.Category;

public interface CategoryRepository extends GraphRepository<Category> {

}
