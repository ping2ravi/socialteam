package com.next.social.team.repo;

import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.GraphRepository;

import com.next.social.team.nodes.Tweet;
import com.next.social.team.nodes.TwitterAccount;
import com.next.social.team.relations.TweetMention;

public interface TweetMentionRepository extends GraphRepository<TweetMention> {
	
    @Query("start twitterAccount=node({0}), tweet=node({1}) match (twitterAccount)<-[tweetMention:TWEET_MENTION]-(tweet) return tweetMention")
    TweetMention getUserTweetMention(TwitterAccount twitterAccount, Tweet tweet);

}
