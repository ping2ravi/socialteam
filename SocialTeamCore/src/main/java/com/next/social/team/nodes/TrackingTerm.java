package com.next.social.team.nodes;

import org.springframework.data.neo4j.annotation.Indexed;
import org.springframework.data.neo4j.annotation.NodeEntity;


@NodeEntity
public class TrackingTerm extends BaseNode {

    @Indexed(indexName = "term")
    private String term;

    @Indexed(indexName = "termCap")
    private String termCap;

    private String trackingStartDate;

    public String getTerm() {
        return term;
    }

    public void setTerm(String term) {
        this.term = term;
    }

    public String getTermCap() {
        return termCap;
    }

    public void setTermCap(String termCap) {
        this.termCap = termCap;
    }

    public String getTrackingStartDate() {
        return trackingStartDate;
    }

    public void setTrackingStartDate(String trackingStartDate) {
        this.trackingStartDate = trackingStartDate;
    }

}
