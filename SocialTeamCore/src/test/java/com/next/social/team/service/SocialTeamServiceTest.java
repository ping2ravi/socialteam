package com.next.social.team.service;

import java.io.InputStream;
import java.io.InputStreamReader;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import com.next.social.team.exceptions.AppException;
import com.next.social.team.repo.service.SocialTeamService;
import com.next.social.team.util.TwitterUtil;

@ContextConfiguration(locations = { "classpath:socialteam-core-test.xml" })
@RunWith(SpringJUnit4ClassRunner.class)
public class SocialTeamServiceTest {

    @Autowired
    private SocialTeamService socialTeamService;

    @Autowired
    private TwitterUtil twitterUtil;

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    public void completeTest() throws AppException {
        Assert.assertNotNull("Test Json is missing", getClass().getResource("/json/retweet.json"));
        JsonParser jsonParser = new JsonParser();
        InputStream fileInputStream = getClass().getResourceAsStream("/json/retweet.json");
        JsonObject tweetJson = jsonParser.parse(new InputStreamReader(fileInputStream)).getAsJsonObject();
        System.out.println("tweetJson : " + tweetJson.toString());

        socialTeamService.importTweet(tweetJson);

    }


    @Test
    public void test() throws InterruptedException, JsonSyntaxException, AppException {
        socialTeamService.getTwitterAccountForHashTag("AAP");
    }

    public void downloadTwitterData() throws InterruptedException, JsonSyntaxException, AppException {

        JsonParser jsonParser = new JsonParser();
        int count = 1;
        while (true) {
            String msg = twitterUtil.getNextTweetMessage();
            if (msg == null) {
                Thread.sleep(1000);
                continue;
            }
            logger.info(count + ".");
            count++;
            socialTeamService.importTweet(jsonParser.parse(msg).getAsJsonObject());
        }
    }
}
